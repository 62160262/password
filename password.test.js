const { checkLength, checkAlphabet, checkDigit, checkSymbol } = require("./password");

describe('Test Password Length', () => {
  test('should 8 charactors to be true', () => {
    expect(checkLength('12345678')).toBe(true);
  });
  test('should 7 charactors to be false', () => {
    expect(checkLength('1234567')).toBe(false);
  });
  test('should 25 charactors to be true', () => {
    expect(checkLength('111111111111111111111111')).toBe(true);
  });
  test('should 26 charactors to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false);
  });

})
describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true);
  });
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true);
  });
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1')).toBe(false);
  });

});
describe('Test Digit', () => {
  test('should has 0-9 in password', () => {
    expect(checkDigit('11')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('1234')).toBe(true);
  });
  test('should has not 0-9 in password', () => {
    expect(checkDigit('n')).toBe(false);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('12')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('13')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('14')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('15')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('16')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('17')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('18')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('19')).toBe(true);
  });
  test('should has 0-9 in password', () => {
    expect(checkDigit('05')).toBe(true);
  });
})
describe('Test Symbol', () => {
  test('should has symbol ! in password ', () => {
    expect(checkSymbol('11!11')).toBe(true)
  });
  test('should has symbol $ in password ', () => {
    expect(checkSymbol('111$1')).toBe(true)
  });
  test('should has symbol " in password ', () => {
    expect(checkSymbol('11123"1')).toBe(true)
  });
  test('should has not symbol in password ', () => {
    expect(checkSymbol('111231')).toBe(true)
  });

})
